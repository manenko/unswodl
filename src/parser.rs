// SPDX-License-Identifier: Apache-2.0

// -----------------------------------------------------------------------------
// Copyright 2019-2020 Oleksandr Manenko
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------

mod parser;

pub use parser::Parser;
pub use parser::Rule;

pub use pest::Parser as PestParser;

use pest::iterators::{Pairs, Pair};
use pest;
use std::str::FromStr;

#[derive(Debug)]
pub enum Literal {
    String(String),
    Integer(i64),
    Float(f64),
    Boolean(bool),
    //TODO: List(???),
    Null
}


#[derive(Debug)]
pub enum Expression {
    Literal(Literal)
}

#[derive(Debug)]
pub struct RetryWhileLoop {
    delay: Option<Box<Expression>>,
    body: Option<Box<Statement>>,
    condition: Box<Expression>,
}

#[derive(Debug)]
pub enum Statement {
    RetryWhileLoop(RetryWhileLoop)
}

impl Literal {
    pub fn parse(pair: Pair<Rule>) -> Self {
        assert!(pair.as_rule() == Rule::Literal);

        let pair = pair.into_inner().next().unwrap();
        match pair.as_rule() {
            Rule::BooleanLiteral => Literal::Boolean(bool::from_str(pair.as_str()).unwrap()),
            Rule::NullLiteral    => Literal::Null,
            Rule::StringLiteral  => Literal::String(pair.into_inner().next().unwrap().as_str().into()),
            Rule::NumericLiteral => {
                let pair = pair.into_inner().next().unwrap();
                match pair.as_rule() {
                    Rule::FloatLiteral => Literal::Float(f64::from_str(pair.as_str()).unwrap()),
                    Rule::DecimalIntegerLiteral => Literal::Integer(i64::from_str(pair.as_str()).unwrap()),
                    _ => unreachable!(),
                }
            }
            _ => unreachable!(),
        }
    }
}

impl RetryWhileLoop {
    pub fn parse(pair: Pair<Rule>) -> Self {
        assert!(pair.as_rule() == Rule::RetryWhileLoop);

        let mut delay = None;
        let mut body = None;
        let mut condition = None;

        for p in pair.into_inner() {
            match p.as_rule() {
                Rule::RetryWhileLoopDelay     => delay     = Some(Expression::parse(p.into_inner().next().unwrap())),
                Rule::RetryWhileLoopBody      => body      = Some(Statement::parse(p.into_inner().next().unwrap())),
                Rule::RetryWhileLoopCondition => condition = Some(Expression::parse(p.into_inner().next().unwrap())),
                _                             => unreachable!(),
            }
        }

        Self {
            delay:     delay.map(Box::new),
            body:      body.map(Box::new),
            condition: condition.map(Box::new).unwrap(),
        }
    }
}

impl Expression {
    pub fn parse(pair: Pair<Rule>) -> Self {
        //println!("{:#?}", pair);
        match pair.as_rule() {
            Rule::Literal => Expression::Literal(Literal::parse(pair)),
            _ => unreachable!(),
        }
    }
}

impl Statement {
    pub fn parse(pair: Pair<Rule>) -> Self {
        match pair.as_rule() {
            Rule::RetryWhileLoop => Statement::RetryWhileLoop(RetryWhileLoop::parse(pair)),
            _ => unreachable!(),
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn integer_literal() {
        let source = "-321123";
        let ast = Parser::parse(Rule::Literal, source).unwrap().next().unwrap();
        let literal = Literal::parse(ast);

        let value = match literal {
            Literal::Integer(i) => i,
            _ => unreachable!(),
        };
        assert_eq!(value, -321123);
    }

    #[test]
    fn float_literal() {
        let source = "-32.421";
        let ast = Parser::parse(Rule::Literal, source).unwrap().next().unwrap();
        let literal = Literal::parse(ast);

        let value = match literal {
            Literal::Float(i) => i,
            _ => unreachable!(),
        };
        assert_eq!(value, -32.421);
    }

    #[test]
    fn boolean_literal() {
        let source = "true";
        let ast = Parser::parse(Rule::Literal, source).unwrap().next().unwrap();
        let literal = Literal::parse(ast);

        let value = match literal {
            Literal::Boolean(i) => i,
            _ => unreachable!(),
        };
        assert_eq!(value, true);
    }

    #[test]
    fn string_literal() {
        let source = "\"Hello, world!\"";
        let ast = Parser::parse(Rule::Literal, source).unwrap().next().unwrap();
        let literal = Literal::parse(ast);

        let value = match literal {
            Literal::String(i) => i,
            _ => unreachable!(),
        };
        assert_eq!(value, "Hello, world!");
    }

    #[test]
    fn null_literal() {
        let source = "null";
        let ast = Parser::parse(Rule::Literal, source).unwrap().next().unwrap();
        let literal = Literal::parse(ast);

        let value = match literal {
            Literal::Null => true,
            _ => false,
        };
        assert_eq!(value, true);
    }

    #[test]
    fn retry_while_loop_empty_body() {
        let source = "retry (5) { } while (false);";
        let ast = Parser::parse(Rule::RetryWhileLoop, source).unwrap().next().unwrap();
        let rw_loop = RetryWhileLoop::parse(ast);

        match *rw_loop.condition {
            Expression::Literal(literal) => {
                match literal {
                    Literal::Integer(value) => assert_eq!(value, 5),
                    _ => unreachable!(),
                }
            },
            _ => unreachable!(),
        };
    }
}


/*
     let source = r#"
     retry (RETRYMAX) {
         var a      = 5;
         var status = MakeMeHappy(1, 2, 3) @ Happiness;
     } while (!status);
     "#;


     Statement::Retry {
         delay: Expression::Identifier("RETRYMAX"),
         body: [
             Statement::VariableDeclaration { name = "a", value = Some(Expression::Literal(Literal::Integer(5))) },
             Statement::VariableDeclaration {
                 name = "status",
                 value = Some(Expression::WebMethodCall {
                     name      = "MakeMeHappy",
                     target    = Some("Happiness"),
                     arguments = [
                         Expression::Literal(Literal::Integer(1)),
                         Expression::Literal(Literal::Integer(2)),
                         Expression::Literal(Literal::Integer(3)),
                     ]
                 })
             },
         ],
         condition: Expression::LogicalNot(Expression::Identifier("status"))
     }
     */


