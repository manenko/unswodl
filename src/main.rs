// SPDX-License-Identifier: Apache-2.0

// -----------------------------------------------------------------------------
// Copyright 2019-2020 Oleksandr Manenko
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------

#[macro_use]
extern crate pest_derive;
mod parser;

use parser::*;
use std::fs;

fn main() {
    let script = fs::read_to_string("test_grammar.wf").unwrap();
    let ast = Parser::parse(Rule::Script, &script);

    match ast {
        Ok(ast) => println!("{:#?}", ast),
        Err(err) => eprintln!("{:#?}", err),
    }
}
